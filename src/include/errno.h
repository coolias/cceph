#ifndef CCEPH_ERRNO_H
#define CCEPH_ERRNO_H

#define CCEPH_OK                    0
#define CCEPH_ERR_UNKNOWN         (-1000)
#define CCEPH_ERR_CONN_NOT_FOUND  (-1001)
#define CCEPH_ERR_CONN_CLOSED     (-1002)
#define CCEPH_ERR_WRITE_CONN_ERR  (-1003)
#define CCEPH_ERR_UNKNOWN_OP      (-1003)

#endif
